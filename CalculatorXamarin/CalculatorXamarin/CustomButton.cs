﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CalculatorXamarin
{
    class CustomButton : SKCanvasView
    {
        public static readonly BindableProperty BackGroundColorProperty = BindableProperty.Create("BackGroundColor", typeof(Color), typeof(CustomButton), Color.FromHex("343434"));
        public static readonly BindableProperty StrokeColorPropery = BindableProperty.Create("StrokeColor", typeof(Color), typeof(CustomButton), Color.FromHex("585858"));
        public static readonly BindableProperty TextColorProperty = BindableProperty.Create("TextColor", typeof(Color), typeof(CustomButton), Color.FromHex("BDC3C7"));
        public static readonly BindableProperty CornerRadiusScaleProperty = BindableProperty.Create("CornerRadiusScale", typeof(float), typeof(CustomButton), 1f);
        public static readonly BindableProperty TextProperty = BindableProperty.Create("Text", typeof(string), typeof(CustomButton), "1");
        public static readonly BindableProperty StrokeWidthProperty = BindableProperty.Create("StrokeWidth", typeof(float), typeof(CustomButton), 1f);
        public static readonly BindableProperty TextScaleProperty = BindableProperty.Create("TextScale", typeof(float), typeof(CustomButton), 0.3f);
        public static readonly BindableProperty DestenyValueProperty = BindableProperty.Create("DestenyValue", typeof(string), typeof(CustomButton), "");

        public Color BackGroundColor
        {
            get
            {
               
               return (Color)GetValue(BackGroundColorProperty);
            }
            set
            {
                SetValue(BackGroundColorProperty, value);
            }
        }
        public Color StrokeColor
        { 
            get
            { 
                return (Color)GetValue(StrokeColorPropery); 
            }
            set
            { 
                SetValue(StrokeColorPropery, value);
            } 
        }
        public Color TextColor 
        {
            get
            {
                return (Color)GetValue(TextColorProperty);
            }
            set
            {
                SetValue(TextColorProperty, value);
            }
        }
        public float CornerRadiusScale 
        { 
            get
            {
                return (float)GetValue(CornerRadiusScaleProperty);
            }
            set
            {
                SetValue(CornerRadiusScaleProperty, value);
            }
        }
        public string Text 
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }
        public string DestenyValue
        { 
            get
            {
                return (string)GetValue(DestenyValueProperty);
            }
            set
            {
                SetValue(DestenyValueProperty, value);
            }
        }
        public float StrokeWidth 
        {
            get
            {
                return (float)GetValue(StrokeWidthProperty);
            }
            set
            {
                SetValue(StrokeWidthProperty, value);
            }
        }
        public float TextScale
        { 
            get
            {
                return (float)GetValue(TextScaleProperty);
            }
            set
            {
                SetValue(TextScaleProperty, value);
            }
        }

        public SKPoint BoxScale { get; set; } = new SKPoint(0.9f, 0.9f);
        public SKPoint TextLocation { get; set; } = new SKPoint(0, 0);

        public delegate void Click(object sender, EventArgs e);
        public event Click Clicked;



        public CustomButton()
        {
            var tapRecog = new TapGestureRecognizer();
            tapRecog.Tapped += (sender, e) => { Clicked?.Invoke(sender, e); };
            GestureRecognizers.Add(tapRecog);
        }

        private void _onTapped(object sender,EventArgs e)
        {

            Clicked?.Invoke(sender, e);
        }

        protected override void OnPaintSurface(SKPaintSurfaceEventArgs args)
        {
            var surFace = args.Surface;
            var info = args.Info;
            var canvas = surFace.Canvas;


            var StrokeBrush = new SKPaint
            {
                Color = StrokeColor.ToSKColor(),
                Style = SKPaintStyle.Stroke,
                StrokeWidth = StrokeWidth
            };
            var BackgroundBrush = new SKPaint
            {
                Color = BackGroundColor.ToSKColor(),
                Style = SKPaintStyle.Fill
            };

            var TextBrush = new SKPaint
            {
                Color = TextColor.ToSKColor(),
                Style = SKPaintStyle.Fill,
                TextSize = info.Height * TextScale,
                TextAlign = SKTextAlign.Center
            };

           
            var Rect = new SKRoundRect();

            var baseRect = new SKRect();
            baseRect.Size = new SKSize(info.Width * BoxScale.X, info.Height * BoxScale.Y);
            baseRect.Location = new SKPoint(0, 0);

            Rect.SetRect(baseRect);
            var CornerRadius = new SKPoint(CornerRadiusScale * info.Width, CornerRadiusScale * info.Height);
            Rect.SetRectRadii(baseRect, new SKPoint[] { CornerRadius, CornerRadius, CornerRadius, CornerRadius });


            canvas.Clear();
            canvas.Save();

            
            canvas.DrawRoundRect(Rect, BackgroundBrush);
            canvas.DrawRoundRect(Rect, StrokeBrush);
            
            canvas.DrawText(Text, info.Width/2-info.Height*TextScale/4, info.Height*(1+TextScale/2)/2, TextBrush);
        }
    }
}
