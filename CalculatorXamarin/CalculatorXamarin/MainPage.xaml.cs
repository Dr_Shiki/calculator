﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Data;

namespace CalculatorXamarin
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        float _width = -1;
        float _height = -1;
        Grid _tmpGrid = new Grid();
        public MainPage()
        {
            InitializeComponent();
        }
        List<SKCanvasView> canvasList = new List<SKCanvasView>();
        void initUIlist()
        {

        }

        void OnClicked(object sender,EventArgs e)
        {
            TextBox_res.Text += ((CustomButton)sender).Text;
        }

        void onCorrect(object sender,EventArgs e)
        {
            if (TextBox_res.Text.Length == 0)
                return;
            var curText = TextBox_res.Text;
            TextBox_res.Text = curText.Remove(curText.Length - 1);
        }
        void OnEqual(object sender,EventArgs e)
        {
            var exp = TextBox_res.Text;
            if (exp.Contains("X"))
                exp = exp.Replace("X", "*");
            try
            {
                Textbox.Text = Convert.ToString((new DataTable()).Compute(exp, null));
            }
            catch (Exception ex)
            {
                Textbox.Text = ex.Message;
            }
            TextBox_res.Text = "";
        }
        void onErase(object sender,EventArgs e)
        {
            Textbox.Text = "";
            TextBox_res.Text = "";
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (width != _width || height != _height)
            {
                _width = (float)width;
                _height = (float)height;
                if(width>height)
                {
                    BaseGrid.ColumnDefinitions.Clear();
                    BaseGrid.RowDefinitions.Clear();

                    BaseGrid.RowDefinitions.Add(new RowDefinition { Height =new GridLength(1, GridUnitType.Star) });

                    BaseGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                    BaseGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                    _tmpGrid.ColumnDefinitions.Clear();
                    _tmpGrid.RowDefinitions.Clear();
                    _tmpGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                    _tmpGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(5, GridUnitType.Star) });
                    _tmpGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(4, GridUnitType.Star) });

                    _tmpGrid.Children.Add(Textbox, 0, 0);
                    _tmpGrid.Children.Add(subGrid_1, 0, 1);

                    BaseGrid.Children.Clear();
                    BaseGrid.Children.Add(subGrid_2, 1, 0);
                    BaseGrid.Children.Add(_tmpGrid, 0, 0);
                }
                else
                {
                    BaseGrid.ColumnDefinitions.Clear();
                    BaseGrid.RowDefinitions.Clear();
                    BaseGrid.Children.Clear();

                    BaseGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(17, GridUnitType.Star) });
                    BaseGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(5, GridUnitType.Star) });
                    BaseGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(28, GridUnitType.Star) });
                    BaseGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                    BaseGrid.Children.Add(Textbox, 0, 0);
                    BaseGrid.Children.Add(subGrid_1, 0, 1);
                    BaseGrid.Children.Add(subGrid_2, 0, 2);
                }
            }
        }
    }
}
